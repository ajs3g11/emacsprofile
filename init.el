(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes (quote (zenburn)))
 '(custom-safe-themes
   (quote
    ("b0e6f38facd8c591991db60389b8b4a98885e7750ed5761ce987bff0588472d3" "afbb40954f67924d3153f27b6d3399df221b2050f2a72eb2cfa8d29ca783c5a8" "9459d3fdc79216e60b509ee3e9d09aa7baff71b032fd9fc94a8957e8c749d8de" default)))
 '(inhibit-startup-screen t)
 '(org-startup-truncated nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(require 'package) ;; You might already have this line
(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/"))

;; Standard package.el + MELPA setup
;; (See also: https://github.com/milkypostman/melpa#usage)
(require 'package)
(add-to-list 'package-archives
	     '("melpa" . "http://melpa.milkbox.net/packages/") t)
(package-initialize)

(when (< emacs-major-version 24)
  ;; For important compatibility libraries like cl-lib
  (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/")))
(package-initialize) ;; You might already have this line

(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/") 

;; following 2 lines include auto-complete
;;(require 'auto-complete)  
;;(global-auto-complete-mode t)

;; following 5 lines include auto-complete and make it so that tab no longer activates auto-complete
;;(require 'auto-complete-config) 
;;(ac-config-default)
;;(add-to-list 'ac-modes 'vhdl-mode) ;; add vhdl mode to modes in which autocomplete is active
;;(define-key ac-mode-map (kbd "TAB") nil)
;;(define-key ac-completing-map (kbd "TAB") nil)
;;(define-key ac-completing-map [tab] nil)
;;
;;(ac-set-trigger-key "`") ;; sets the auto-complete key
;;
;;(require 'auto-complete-config)
;;(ac-config-default)

(add-to-list 'auto-mode-alist '("\\.pyx\\'" . python-mode))

(defun dot-emacs (relative-path)
  "Return the full path of a file in the user's emacs directory."
  (expand-file-name (concat user-emacs-directory relative-path)))

(add-to-list 'load-path
	                   "~/.emacs.d/elpa/yasnippet")
(require 'yasnippet)
(yas/initialize)
(yas/load-directory
  (dot-emacs "elpa/yasnippet/snippets"))

;; Remove Yasnippet's default tab key binding
(define-key yas-minor-mode-map (kbd "<tab>") nil)
(define-key yas-minor-mode-map (kbd "TAB") nil)
;; Set Yasnippet's key binding to shift+tab
(define-key yas-minor-mode-map (kbd "§") 'yas-expand)

(setq column-number-mode t) ;; emacs displays column number as well

(load-theme 'zenburn)

(setq show-paren-delay 0)
(show-paren-mode 1)

;;(global-aggressive-indent-mode 1) ;; enable aggressive-indent-mode everywhere

(add-hook 'after-init-hook 'global-company-mode) ;; enable company-mode everywhere

(global-set-key "`" 'company-complete-common)

(add-to-list 'auto-mode-alist '("\\.cu$" . c-mode))

;; Standard Jedi.el setting
(add-hook 'python-mode-hook 'jedi:setup)
(setq jedi:complete-on-dot t)

(add-hook 'TeX-mode-hook 'visual-line-mode)
(add-hook 'TeX-mode-hook 'reftex-mode)
(add-hook 'LaTeX-mode-hook (function turn-on-reftex))
(setq reftex-plug-into-AUCTeX t)
